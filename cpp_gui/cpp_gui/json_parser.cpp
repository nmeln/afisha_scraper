#include <QJsonDocument>
#include <QJsonArray>
#include <QFile>

QJsonArray parseJsonToArray(const QString &file_path){
    QFile file(file_path);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QByteArray jsonData = file.readAll();
    file.close();

    QJsonDocument events = QJsonDocument::fromJson(jsonData);

    return events.array();
}

