TEMPLATE = app

QT += qml quick widgets

SOURCES += main.cpp \
    dataobject.cpp \
    json_parser.cpp \
    comboboxsignalreceiver.cpp \
    utility_funcs.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    dataobject.h \
    json_parser.h \
    comboboxsignalreceiver.h \
    utility_funcs.h
