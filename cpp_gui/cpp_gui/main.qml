import QtQuick 2.0
import QtQuick.Controls 1.1
import QtQuick.Dialogs 1.2
import QtQuick.Window 2.0

Rectangle {
    width: 510
    height: 400
    color: "#fdfdfd"
    clip: true



    ListView {
        id: eventListView
        x: 8
        y: 40
        width: 494
        height: 350
        contentHeight: 325
        contentWidth: 0
        cacheBuffer: 340
        layoutDirection: Qt.LeftToRight
        orientation: ListView.Vertical
        keyNavigationWraps: true
        spacing: 55
        snapMode: ListView.SnapToItem

        model: eventListModel

        delegate: Item {
            id: item1
            x: 0
            width: 494
            height: 140
            Row {

                id: row
                x: 0
                y: 0
                width: 386
                height: 120
                spacing: 10

                Rectangle{
                    width: 494
                    height: 140
                    color: "#ffffff"
                    radius: 7
                    border.color: "#6cd8cb"
                    border.width: 2
                    antialiasing: true

                    MouseArea {
                        id: mouse_area1
                        x: 8
                        y: 28
                        width: imgRect.width
                        height: imgRect.height
                        Image{
                            id: imgRect
                            x: 0
                            y: 0
                            width: 60
                            height: 85
                            source: model.modelData.thumb
                            scale:  mouse_area1.containsMouse ? 1.1 : 1.0

                        }
                        hoverEnabled: true
                        onClicked: Qt.openUrlExternally(model.modelData.link)
                    }

                    Text {
                        id: nametext
                        y: 7
                        width: 403
                        height: 32
                        text: model.modelData.name
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        anchors.left: parent.left
                        anchors.leftMargin: 83
                        font.bold: true
                        font.family: "PT Sans"
                        font.pointSize: 12
                    }
                    Text {
                        id: desctext
                        width: 403
                        height: 30
                        text: model.modelData.desc
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        clip: false
                        anchors.left: parent.left
                        anchors.leftMargin: 83
                        anchors.top: nametext.bottom
                        anchors.topMargin: 11
                        font.family: "PT Sans"
                        font.bold: false
                    }
                    Text {
                        id: datetext
                        width: 127
                        height: 13
                        text: model.modelData.date
                        anchors.left: parent.left
                        anchors.leftMargin: 84
                        anchors.top: desctext.bottom
                        anchors.topMargin: 6
                        font.family: "PT Sans"
                        font.bold: false
                    }
                    Text {
                        id: placetext
                        width: 150
                        text: model.modelData.place
                        anchors.topMargin: 6
                        font.bold: false
                        anchors.top: datetext.bottom
                        anchors.left: parent.left
                        font.family: "PT Sans"
                        anchors.leftMargin: 84
                    }
            }
            }
        }


    }


    Image {
        id: menubackgroundimage
        x: 0
        y: 0
        width: 510
        height: 34
        fillMode: Image.Stretch
        asynchronous: true
        source: "ui_images/menubackgroundimage.jpg"

        ComboBox {
            id: comboBox1
            objectName: "combo"
            model: comboBoxModel
            currentIndex: 0
            x: 418
            y: 8
            width: 84
            height: 20
            activeFocusOnPress: true
        }
    }

    Text {
        id: text1
        x: 0
        y: 388
        text: qsTr("0.6")
        font.family: "Tahoma"
        font.pixelSize: 10
    }

}
