#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QQmlContext>
#include <QQuickView>

#include "json_parser.h"
#include "dataobject.h"

extern QStringList event_types;
extern QQuickView* glob_view;

QList<QObject*> formDataList(const QJsonArray &events_arr){
    QList<QObject*> dataList;

    foreach (const QJsonValue & value, events_arr)
    {
            QJsonObject obj = value.toObject();
            dataList.append(new DataObject(obj["name"].toString(), obj["desc"].toString(), obj["date"].toString(),
                    obj["place"].toString(), obj["thumb"].toString(), obj["link"].toString()));
    }
    return dataList;
}

void change_data_model(const int &value){
    QStringList event_types;

    event_types.append("concerts");
    event_types.append("exhibitions");
    event_types.append("cinema");
    event_types.append("parties");
    event_types.append("performance");
    event_types.append("presentations");
    event_types.append("inUkraine");

    QJsonArray events_arr = parseJsonToArray(event_types[value] + ".json"); //use received value to determine selected event type
    QList<QObject*> dataList = formDataList(events_arr);
    QQmlContext *context = glob_view->rootContext();
    context->setContextProperty("eventListModel",QVariant::fromValue(dataList)); //update/change data model of ListView
}
