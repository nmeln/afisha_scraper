#ifndef JSON_PARSER_H
#define JSON_PARSER_H

QJsonArray parseJsonToArray(const QString &file_path);

#endif // JSON_PARSER_H
