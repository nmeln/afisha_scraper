#include "dataobject.h"

DataObject::DataObject(QObject *parent) :
    QObject(parent)
{
}


DataObject::DataObject(const QString &name, const QString &desc, const QString &date,  const QString &place,
           const QString &thumb, const QString &link,  QObject *parent)
    : QObject(parent), m_name(name), m_desc(desc), m_date(date), m_place(place),
      m_thumb(thumb), m_link(link)
{
}

QString DataObject::name() const
{
    return m_name;
}


QString DataObject::desc() const
{
    return m_desc;
}


QString DataObject::date() const
{
    return m_date;
}

QString DataObject::place() const
{
    return m_place;
}

QString DataObject::thumb() const
{
    return m_thumb;
}

QString DataObject::link() const
{
    return m_link;
}

void DataObject::setName(const QString &name)
{
    if (name != m_name) {
        m_name = name;
        emit nameChanged();
    }
}
