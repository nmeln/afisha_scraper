#ifndef COMBOBOXSIGNALRECEIVER_H
#define COMBOBOXSIGNALRECEIVER_H

#include <QObject>
#include <iostream>

class ComboBoxSignalReceiver : public QObject
{
    Q_OBJECT
public slots:
    void cppSlot(const int &v);
};
#endif // COMBOBOXSIGNALRECEIVER_H
