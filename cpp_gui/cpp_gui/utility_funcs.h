#ifndef UTILITY_FUNCS_H
#define UTILITY_FUNCS_H

void change_data_model(const int &value);
QList<QObject*> formDataList(const QJsonArray &events_arr);
#endif // UTILITY_FUNCS_H
