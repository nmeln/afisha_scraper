import tempfile
from bs4 import BeautifulSoup
import requests
import json

afisha_event_types = ['performance', 'inUkraine', 'concerts', 'parties',
                      'cinema', 'presentations', 'exhibitions']


def get_page():
    r = requests.get(r"http://afishalviv.net/page/events/month/")
    soup_page = BeautifulSoup(r.text)
    event_list = soup_page.find("ul", id="event_list")
    return event_list

def extract_info(item):
    """:rtype dict"""
    #info_dictionary = dict.fromkeys(["thumb", "link", "name", "desc", "date"])
    info_dictionary = {}
    info_dictionary["thumb"] = "http://afishalviv.net/" + item.img.get('src')  # small picture
    info_dictionary["link"] = item.h3.a.get('href')
    info_dictionary["name"] = item.h3.a.get_text()
    info_dictionary["desc"] = item.p.get_text()
    info_dictionary["date"] = item.p.find_next().get_text().split(',')[0]
    place = "".join(item.p.find_next().get_text().split(',')[1:])
    info_dictionary["place"] = place.strip()

    return info_dictionary


def item_generator(event_list, event_type):
    """
    :returns data about the event
    :rtype : dict
    """

    for item in event_list.find_all('li', class_=event_type):
        yield extract_info(item)


if __name__ == '__main__':


    events_list = get_page()
    for event_type in afisha_event_types:
        specific_events_list = list(item_generator(events_list, event_type))
        with open(event_type + ".json", "w") as output_file:
            json.dump(specific_events_list, output_file)
