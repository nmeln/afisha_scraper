#include <QQuickView>
#include <QQmlContext>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QListView>
#include <QtQuick>
#include <QFontDatabase>
#include <iostream>

#include <dataobject.h>
#include <json_parser.h>
#include <comboboxsignalreceiver.h>
#include <utility_funcs.h>

QList<QObject*> formDataList(const QJsonArray &events_arr);


QQuickView *glob_view;

int main(int argc, char *argv[])
{

    QApplication app(argc, argv);
    QQuickView view;
    view.setSource(QUrl("qrc:main.qml"));
    view.setResizeMode(QQuickView::SizeRootObjectToView);

    glob_view = &view;

    QProcess *process = new QProcess();
    QString file = "scraper/scraper.exe"; // path to python script exe
    process->start(file);
    process->waitForFinished();

    QJsonArray usedEventArr = parseJsonToArray("concerts.json"); //default eventArray
    QList<QObject*> dataList = formDataList(usedEventArr); //form QList to use for the data model

    QQmlContext *context = view.rootContext();

    QStringList eventsNames;
    eventsNames.append("Концерти");
    eventsNames.append("Виставки");
    eventsNames.append("Кіно");
    eventsNames.append("Вечірки");
    eventsNames.append("Спектаклі");
    eventsNames.append("Презентації");
    eventsNames.append("В Україні");

    context->setContextProperty("eventListModel",QVariant::fromValue(dataList)); //creating data model from QList of events
    context->setContextProperty("comboBoxModel", QVariant::fromValue(eventsNames)); //creating data model for combobox


    ComboBoxSignalReceiver comboBoxSignalReceiver;

    QQuickItem *combo = view.rootObject()->findChild<QQuickItem*>("combo");
    if(combo) {
    QObject::connect(combo, SIGNAL(activated(int)),
            &comboBoxSignalReceiver, SLOT(cppSlot(int))); //if user selects another item in combobox
    }

    QFontDatabase::addApplicationFont(":/fonts/PT_Sans-Web-Bold.ttf");
    QFontDatabase::addApplicationFont(":/fonts/PT_Sans-Web-Regular.ttf");

    view.setTitle("Афіша Львова");
    view.setIcon(QIcon(":/ui_images/navigation-icon.png")); //relative path to the icon in the resources
    view.setMaximumSize(QSize(510, 400));
    view.setMinimumSize(QSize(510, 400));
    view.show();

    return app.exec();
}



