#include "comboboxsignalreceiver.h"
#include "utility_funcs.h"

void ComboBoxSignalReceiver::cppSlot(const int &v) {
   change_data_model(v);
   //user changed selection in combobox
   //slot receives the integer value of the selection
}

