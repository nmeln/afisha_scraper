#ifndef DATAOBJECT_H
#define DATAOBJECT_H

#include <QObject>



class DataObject : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString desc READ desc)
    Q_PROPERTY(QString date READ date)
    Q_PROPERTY(QString place READ place)
    Q_PROPERTY(QString thumb READ thumb)
    Q_PROPERTY(QString link READ link)

public:
    DataObject(QObject *parent=0);
    DataObject(const QString &name, const QString &desc, const QString &date,  const QString &place,
               const QString &thumb, const QString &link,  QObject *parent=0);

    QString name() const;
    QString desc() const;
    QString date() const;
    QString place() const;
    QString thumb() const;
    QString link() const;
    void setName(const QString &name);


signals:
    void nameChanged();


private:
    QString m_name;
    QString m_desc;
    QString m_date;
    QString m_place;
    QString m_thumb;
    QString m_link;
};

#endif // DATAOBJECT_H
